/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.Test ;
import static org.junit.Assert.* ;
import OxNetBean.Player;

/**
 *
 * @author Taneat
 */
public class PlayerUnitTest {
    
    public PlayerUnitTest() {
    }
    
@Test
    public void getCurrentPlayer(){
        Player p = new Player('o');
        p.getName();
        assertEquals('o',p.getName());
        
    }
@Test
    public void countWin(){
        Player p = new Player('o');
        p.countWin();
        assertEquals(1,p.getWin());
   
    }
@Test
    public void countLose(){
        Player p = new Player('o');
        p.countLose();
        assertEquals(1,p.getLose());
   
    }
@Test
    public void countDarw(){
        Player p = new Player('o');
        p.countDraw();
        assertEquals(1,p.getDraw());
   
    }    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
