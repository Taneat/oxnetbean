/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.Test ;
import static org.junit.Assert.* ;
import OxNetBean.Board;
import OxNetBean.Player;
/**
 *
 * @author Taneat
 */
public class BoardUnitTest {
    
     public BoardUnitTest() {
    }
@Test
    public void testRow1Win(){
        Player o =new Player('o');
        Player x =new Player('x');
        Board board = new Board(o,x);
        board.setRowCol(1, 1);
        board.setRowCol(1, 2);
        board.setRowCol(1, 3);
        assertEquals(true,board.checkForWin());
    }
@Test
    public void testRow2Win(){
        Player o =new Player('o');
        Player x =new Player('x');
        Board board = new Board(o,x);
        board.setRowCol(2, 1);
        board.setRowCol(2, 2);
        board.setRowCol(2, 3);
        assertEquals(true,board.checkForWin());
    }
@Test
    public void testRow3Win(){
        Player o =new Player('o');
        Player x =new Player('x');
        Board board = new Board(o,x);
        board.setRowCol(3, 1);
        board.setRowCol(3, 2);
        board.setRowCol(3, 3);
        assertEquals(true,board.checkForWin());
    }
@Test
    public void testcol1Win(){
        Player o =new Player('o');
        Player x =new Player('x');
        Board board = new Board(o,x);
        board.setRowCol(1, 1);
        board.setRowCol(2, 1);
        board.setRowCol(3, 1);
        assertEquals(true,board.checkForWin());
    }
@Test
    public void testcol2Win(){
        Player o =new Player('o');
        Player x =new Player('x');
        Board board = new Board(o,x);
        board.setRowCol(1, 2);
        board.setRowCol(2, 2);
        board.setRowCol(3, 2);
        assertEquals(true,board.checkForWin());
    }
@Test
    public void testcol3Win(){
        Player o =new Player('o');
        Player x =new Player('x');
        Board board = new Board(o,x);
        board.setRowCol(1, 3);
        board.setRowCol(2, 3);
        board.setRowCol(3, 3);
        assertEquals(true,board.checkForWin());
    }
@Test
    public void testDiagonal1Win(){
        Player o =new Player('o');
        Player x =new Player('x');
        Board board = new Board(o,x);
        board.setRowCol(1, 1);
        board.setRowCol(2, 2);
        board.setRowCol(3, 3);
        assertEquals(true,board.checkForWin());
    }
@Test
    public void testDiagonal2Win(){
        Player o =new Player('o');
        Player x =new Player('x');
        Board board = new Board(o,x);
        board.setRowCol(1, 3);
        board.setRowCol(2, 2);
        board.setRowCol(3, 1);
        assertEquals(true,board.checkForWin());
    }
@Test
    public void testSwitchPlayer(){
        Player o =new Player('o');
        Player x =new Player('x');
        Board board = new Board(o,x);
        if(board.getCurrentPlayer().getName()== 'o')  {
            board.switchTrun();
            assertEquals('x',board.getCurrentPlayer().getName());
        }else{
            board.switchTrun();
            assertEquals('o',board.getCurrentPlayer().getName());
        }  
    }
}
